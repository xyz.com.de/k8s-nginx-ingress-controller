# k8s Nginx ingress controller
In this example we build a custom ingress controller with nginx. This setups is 
used to run on a singel node k8s.

Create ingress namespace
    
    kubectl create namespace ingress

Create default backend

    kubectl create -f default-backend-deployment.yaml -f default-backend-service.yaml -n=ingress

Create Nginx controller config map

    kubectl create -f nginx-ingress-controller-config-map.yaml -n=ingress

Create RBAC rules https://kubernetes.io/docs/reference/access-authn-authz/rbac/
    
    kubectl create -f nginx-ingress-controller-config-map.yaml -n=ingress

Create ingress rules for applications
    
    kubectl create -f nginx-ingress.yaml -n=ingress

Create Nginx controller service
    
    kubectl create -f nginx-ingress-controller-service.yaml -n=ingress

Configure app for ingress controller
    
    kubectl create -f app-ingress.yaml

Deploy app

    kubectl create -f app-deployment.yaml -f app-service.yaml

Now app should be available by cluster IP with the correct hostname. Now patch the
nginx-ingress service in ingress namespace and set en external IP

    kubectl patch service nginx-ingress -p '{"spec":{"externalIPs":["192.168.15.2"]}}' -n=ingress


    [root@node-1 nginx-ingress-controller]# k get svc -n=ingress
    NAME              TYPE        CLUSTER-IP      EXTERNAL-IP    PORT(S)            AGE
    default-backend   ClusterIP   10.101.146.72   <none>         80/TCP             55m
    nginx-ingress     ClusterIP   10.107.35.186   192.168.15.2   80/TCP,18080/TCP   30m

Sources:

[1] https://akomljen.com/kubernetes-nginx-ingress-controller/

[2] https://weave.works/blog/kubernetes-faq-how-can-i-route-traffic-for-kubernetes-on-bare-metal

[3] https://kubernetes.github.io/ingress-nginx/deploy/baremetal/

[4] https://stackoverflow.com/a/56438668

[5] https://metallb.universe.tf/
